import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'


// import Profile from '../components/UserAccount/AccountDetails.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    // name: 'Home',
    component: Home,
    children: [
      {
        path: '',
        name: 'Home',
        component: () => import('../views/StartPage.vue'),
        meta: {
          layout: 'main-layout'
        }
      },
      {
        path: '/account',
        // name: 'Account',
        component: () => import('../views/UserAccount.vue'),
        // meta: { requiresAuth: true }
        children: [
          {
            path: '',
            name: 'Profile',
            component: () => import('../components/userAccount/AccountDetails.vue')
          },
          {
            path: '/messages',
            name: 'Messages',
            component: () => import('../components/userAccount/Messages.vue')
          }
        ]
      },
      {
        path: '/timeline/:id',
        name: 'TimelineItem',
        component: () => import('../views/TabItem.vue'),
        // children: [
        //   {
        //     path: '/:name',
        //     name: 'Article',
        //     component: () => import('../views/CurrentArticle.vue')
        //   }
        // ]
      },
      {
        path: 'article/:name',
        name: 'Article',
        component: () => import('../views/CurrentArticle.vue')
      },

      {
        path: '/forum',
        name: 'Forum',
        component: () => import('../views/Forum.vue')
      },
      
      {
        path: '/create-post',
        name: 'CreatePost',
        component: () => import('../views/CreatePost.vue')
      },
    ]
  },
  {
    path: '/auth',
    name: 'Auth',
    component: () => import('../views/auth/Authorisation.vue')
  },

  // {
  //   path: '/success',
  //   name: 'Success',
  //   component: () => import('../views/auth/SuccessAuth.vue')
  // },
  {
    path: '/verification',
    name: 'Verification',
    component: () => import('../views/auth/Verification.vue')
  },
  {
    path: '/forgot',
    name: 'ForgotPassword',
    component: () => import('../views/auth/ForgotPassword.vue')
  },
  {
    path: '/new-password',
    name: 'CreateNewPassword',
    component: () => import('../views/auth/CreateNewPassword.vue')
  },

]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

// before router auth ?
// router.beforeEach((to, from, next) => {
//   if(to.matched.some(route => route.meta.requiresAuth)) {
  //  localstorage or coockie &
//     if (store.getters.isLoggedIn) {
//       next()
//       return
//     }
//     next('/login') 
//   } else {
//     next() 
//   }
// })

export default router
