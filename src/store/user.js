
const user = {
  state: {
    user : localStorage.getItem('user') || null,
    status: '',
    authError: '',

    newPasswordSuccessFlag: ''
  },

  getters: {
    isLoggedIn: state => !!state.user,
    status: state => state.status,
    currentUser: state => state.user,
    authError: state => state.authError,


    newPasswordSuccessFlag: state => state.newPasswordSuccessFlag
  },

  mutations: {
    register_success(state, user) {
      state.status = 'registered'
      state.user = user
    },
    auth_success(state, user) {
      state.status = 'loggedIn'
      state.user = user
    },
    auth_error(state, err) {
      state.status = ''
      state.authError = err
    },
    logout(state){
      state.status = ''
      state.user = {}
    },


    // восстановление пароля
    new_password_success(state) {
      state.newPasswordSuccessFlag = true
    }
  },

  actions: {
  //   login({ commit }, user) {
  //     return new Promise((resolve, reject) => {
  //       commit('auth_error', '')
  //       axios({url: 'http://localhost:3000/login', data: user, method: 'POST' })
  //       .then(resp => {
  //         const token = resp.data.token
  //         const user = resp.data.user
  //         localStorage.setItem('user', user)

  //         document.cookie = "Token=" + token + ";" + 'expires' + ";path=/";

  //         commit('auth_success', user)
  //         resolve(resp)
  //       })
  //       .catch(err => {
  //         commit('auth_error', err)
  //         reject(err)
  //       })
  //     })
  //   },

  //   register({commit}, user){
  //     return new Promise((resolve, reject) => {
  //       commit('auth_error', '')
  //       axios({url: 'http://localhost:3000/register', data: user, method: 'POST' })
  //       .then(resp => {
  //         const token = resp.data.token
  //         const user = resp.data.user
          
  //         commit('register_success', user)
  //         resolve(resp)
  //       })
  //       .catch(err => {
  //         commit('auth_error', err)

  //         reject(err)
  //       })
  //     })
  //   },

  logout({commit}) {
    return new Promise((resolve) => {
      commit('logout')
      document.cookie =  "Token=" + '=; Max-Age=-99999999;'; // ?
      localStorage.removeItem('user')
      resolve()
    })
    },




  testLog({commit}, user) {
    return new Promise(resolve => {
      commit('auth_success', user)
      document.cookie = "Token=" + 'token' + ";" + 'expires' + ";path=/";
      localStorage.setItem('user', user)
      resolve(user)
    })
  },
    testreg({commit}, user) {
      return new Promise(resolve => {
        commit('register_success', user)
        localStorage.setItem('user', user)
        resolve(user)
      })
    },



    // восстановление пароля
    createNewPassword({ commit }, password) {
      return new Promise(resolve => {
        console.log(password);
        commit('new_password_success')
        resolve('ok')
      })
    },

    loggedWithGoogle({ commit }, user) {
      commit('auth_success', user)
    }
  }
}

export default user