import convertToHTML from 'markdown-to-html-converter'

const posts = {
  state: {
    allPosts: []
    // user : localStorage.getItem('user') || null,
    // status: '',
    // authError: '',

    // newPasswordSuccessFlag: ''
  },

  getters: {
    allPosts: state => state.allPosts
    // isLoggedIn: state => !!state.user,
    // status: state => state.status,
    // currentUser: state => state.user,
    // authError: state => state.authError,
    // newPasswordSuccessFlag: state => state.newPasswordSuccessFlag
  },

  mutations: {
    // register_success(state, user) {
    //   state.status = 'registered'
    //   state.user = user
    // },
    // auth_success(state, user) {
    //   state.status = 'loggedIn'
    //   state.user = user
    // },
    // auth_error(state, err) {
    //   state.status = ''
    //   state.authError = err
    // },
    // logout(state){
    //   state.status = ''
    //   state.user = {}
    // },

    addNewPost(state, newpost) {
      state.allPosts.push(newpost)
    }
  },

  actions: {
    addPost({ commit }, newpost) {
      return new Promise((resolve) => {        

        // ///
        newpost = {...newpost, post: convertToHTML(newpost.post)}
        // ///

        commit('addNewPost', newpost)
        resolve('crearted')
      })
    }
  //   login({ commit }, user) {
  //     return new Promise((resolve, reject) => {
  //       commit('auth_error', '')
  //       axios({url: 'http://localhost:3000/login', data: user, method: 'POST' })
  //       .then(resp => {
  //         const token = resp.data.token
  //         const user = resp.data.user
  //         localStorage.setItem('user', user)

  //         document.cookie = "Token=" + token + ";" + 'expires' + ";path=/";

  //         commit('auth_success', user)
  //         resolve(resp)
  //       })
  //       .catch(err => {
  //         commit('auth_error', err)
  //         reject(err)
  //       })
  //     })
  //   },

  //   register({commit}, user){
  //     return new Promise((resolve, reject) => {
  //       commit('auth_error', '')
  //       axios({url: 'http://localhost:3000/register', data: user, method: 'POST' })
  //       .then(resp => {
  //         const token = resp.data.token
  //         const user = resp.data.user
          
  //         commit('register_success', user)
  //         resolve(resp)
  //       })
  //       .catch(err => {
  //         commit('auth_error', err)

  //         reject(err)
  //       })
  //     })
  //   },
  }
}

export default posts