import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

import VueToastify from "vue-toastify";
import VueMask from 'v-mask'

import VueGapi from 'vue-gapi'
import VueQuillEditor from 'vue-quill-editor'

import MainLayout from './layouts/MainLayout.vue'

// !!!!! dis is myyy
Vue.use(VueGapi, {
  apiKey: 'AIzaSyDdLtRV8ECSAoDWoIUKQ7OvO3dSXDkbpO0',
  client_id: '756112431460-lk0i4ssn9kebq3on99l47renoinm8uva.apps.googleusercontent.com',
  discoveryDocs: ['https://sheets.googleapis.com/$discovery/rest?version=v4'],
  scope: 'https://www.googleapis.com/auth/spreadsheets',
})


Vue.use('main-layout', MainLayout)
Vue.use(VueToastify, {
  successDuration: 8000,
  warningInfoDuration: 8000,
});
Vue.use(VueMask);

Vue.use(VueQuillEditor)


Vue.config.productionTip = false

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
